-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 24 nov 2020 om 23:23
-- Serverversie: 10.4.14-MariaDB
-- PHP-versie: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `craftingshop_database`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `craftingshop_categories`
--

CREATE TABLE `craftingshop_categories` (
  `id` int(11) NOT NULL,
  `parentId` bigint(20) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `item_id` varchar(8) NOT NULL DEFAULT '54',
  `title_color` varchar(5) NOT NULL,
  `display_formation` varchar(4) NOT NULL DEFAULT 'list'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `craftingshop_products`
--

CREATE TABLE `craftingshop_products` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `price` float NOT NULL,
  `categoryId` int(10) NOT NULL,
  `image` text NOT NULL,
  `GUI_item` varchar(8) NOT NULL DEFAULT '54',
  `servers` text NOT NULL,
  `recurring_payment` varchar(3) NOT NULL,
  `quantity` int(10) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `type` varchar(10) NOT NULL DEFAULT 'ingame'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `craftingshop_categories`
--
ALTER TABLE `craftingshop_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `craftingshop_products`
--
ALTER TABLE `craftingshop_products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `craftingshop_categories`
--
ALTER TABLE `craftingshop_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `craftingshop_products`
--
ALTER TABLE `craftingshop_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
