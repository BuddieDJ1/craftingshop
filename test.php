<?php include 'config/config.include.php';

  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
    $uri = 'https://';
  } else {
    $uri = 'http://';
  }

  $uri .= $_SERVER['HTTP_HOST'];

  if(!defined('APP_NAME') || !defined('SQL_HOST')  || !defined('SQL_USERNAME') || !defined('SQL_PASSWORD') || !defined('SQL_DB') || !defined('LICENSE')) {
    header("Location: .app/installer/index.php");
  } else {
    header("Location: ./app/index.php");
  }

?>
