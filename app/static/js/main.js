    <script>
  paypal.Buttons({

    onInit: function(data, actions) {

        actions.disable();

        var firstname = document.getElementById('firstName')
        var lastname = document.getElementById('lastName')
        var email = document.getElementById('email')
        var address = document.getElementById('address')
        var address2 = document.getElementById('address2')
        var country = document.getElementById('country')
        var state = document.getElementById('state')
        var zip = document.getElementById('zip')

        document.querySelector('#check').addEventListener('change', function(event) {

        if (event.target.checked) {
            if(firstname.value.length == 0 || lastname.value.length == 0 || email.value.length == 0 || address.value.length == 0 || country.value.length == 0 || state.value.length == 0 || zip.value.length == 0) {
                actions.disable();
               document.querySelector('#error').style.display = 'block';
            } else {
                actions.enable();
            }
        } else {
            actions.disable();
        }
        });
    },

    // onClick is called when the button is clicked
    onClick: function() {

      // Show a validation error if the checkbox is not checked
      if (!document.querySelector('#check').checked) {
          document.querySelector('#error').style.display = 'block';
      }
    },
    createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: '{{ price }}'
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(function(details) {
        // This function shows a transaction success message to your buyer.
        alert('Transaction completed by ' + details.payer.name.given_name);
      });
    }

  }).render('#paypal-button-container');

    </script>