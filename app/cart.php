
<?php

  include('./includes/header.php');

?>

<script
    src="https://www.paypal.com/sdk/js?client-id=AaQD3oc_7nF7kEsCFk7qmVQs_15H9vyqF9hNjLNNaJdTJm4DkWWvMZD-YfuBOhy5rdU-uxmlMQ6ukzxW&currency=EUR"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
</script>

  <style>
  input[type=text], input[type=email], select {
      border-radius: 0px;
  }
  </style>

<div class="row">
  <div class="col-md-8">
    <div class="card">
      <div class="card-body">
              <div class="col-md-12 order-md-1">
                  <h4 class="mb-3">Winkelwagen</h4>

              <table id="cart" class="table table-condensed">
          <thead>
          <tr>
            <th style="width: 40%">Product</th>
            <th style="width: 20%">Prijs</th>
            <th style="width: 25%" class="text-center">Aantal</th>
            <th style="width: 15%"></th>
          </tr>
        </thead>
        <tbody>

                  <!-- {% for row in cartdata %} -->

          <tr>
            <td data-th="Product">
              <div class="row">
                <div class="col-sm-10">
                  <h6 class="nomargin">{{ row[0][1] }}</h6>
                </div>
              </div>
            </td>
            <td data-th="Price">€ {{ row[0][2] }},00</td>
            <td data-th="Quantity">
              <input type="number" class="form-control text-center" value="1">
            </td>
            <td class="actions" data-th="">
              <a class="btn btn-info btn-sm" href="/checkout" style="border-radius: 0; background: #325BA6; border-color: #325BA6; color: #ffffff;"><i class="fas fa-sync-alt"></i></a>
              <a class="btn btn-danger btn-sm" href="/removeFromCart?id={{ row[0][0] }}" style="border-radius: 0px; color: #ffffff;"><i class="fas fa-trash-alt"></i></a>
            </td>
          </tr>

                  <!-- {% endfor %} -->
        </tbody>
        <tfoot>
        </tfoot>
      </table>
              <!-- {% if price is defined %} -->
                  <h5 style="float: right">Totaal: € {{ price }},00</h5>
              <!-- {% else %} -->
              <!-- <h5 style="float: right">Totaal: € 0,00</h5> -->
              <!-- {% endif %} -->
                  <br>
                  <h4 class="mb-3">Checkout</h4>
                <form class="needs-validation" novalidate="" action="/checkoutForm" method="POST">
                  <div class="alert alert-danger" id="error" style="display: none; border-radius: 0px" role="alert">
                    Vul alle info in om verder te gaan..
                  </div>
                    <form action="">
                <div class="row">
                    <div class="col-md-6 mb-3">
                      <label for="firstName">Voornaam</label>
                      <input type="text" class="form-control" id="firstName" placeholder="" value="" name="firstname" required>
                      <div class="invalid-feedback">
                        Valid first name is required.
                      </div>
                    </div>
                    <div class="col-md-6 mb-3">
                      <label for="lastName">Achternaam</label>
                      <input type="text" class="form-control" id="lastName" placeholder="" value="" name="lastname" required>
                      <div class="invalid-feedback">
                        Valid last name is required.
                      </div>
                    </div>
                  </div>

                  <div class="mb-3">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="" required>
                    <div class="invalid-feedback">
                      Please enter a valid email address for shipping updates.
                    </div>
                  </div>

                  <div class="mb-3">
                    <label for="address">Adres</label>
                    <input type="text" class="form-control" id="address" name="adress" placeholder="" required>
                    <div class="invalid-feedback">
                      Please enter your shipping address.
                    </div>
                  </div>

                  <div class="mb-3">
                    <label for="address2">Adres 2 <span class="text-muted">(Optioneel)</span></label>
                    <input type="text" class="form-control" id="address2" name="adress2" placeholder="">
                  </div>

                  <div class="row">
                    <div class="col-md-5 mb-3">
                       <label for="zip">Land</label>
                      <input type="text" class="form-control" id="country" name="country" placeholder="" required>
                      <div class="invalid-feedback">
                        Zip code required.
                      </div>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="zip">Provincie</label>
                      <input type="text" class="form-control" id="state" name="state" placeholder="" required>
                      <div class="invalid-feedback">
                        Zip code required.
                      </div>
                    </div>
                    <div class="col-md-3 mb-3">
                      <label for="zip">Postcode</label>
                      <input type="text" class="form-control" id="zip" name="zip" placeholder="" required>
                      <div class="invalid-feedback">
                        Zip code required.
                      </div>
                    </div>
                  </div>
                  <hr class="mb-4">
                      <label><input id="check" type="checkbox"> Ik ga akkoord met de algemene voorwaarden</label>

                       <div id="paypal-button-container"></div>

                  <input type="submit" class="btn btn-primary btn-lg btn-block" style="float: right; border-radius: 0; background: #325BA6; border-color: #325BA6;" value="Ga naar betaling"></input>

                  </form>
                </form>
              </div>
      </div>
    </div>
    <BR>
  </div>
  <div class="col-md-4 order-md-2 mb-4">

    <?php

      if(isset($_SESSION['loggedIn'])) {

    ?>

    <ul class='list-group mb-3' class='recent-orders' style='border-radius: 0px;'>
      <li class='list-group-item d-flex justify-content-start lh-condensed'>
        <div>
          <img src='https://crafatar.com/avatars/<?php echo $_SESSION['uuid']; ?>?size=40&overlay'>
        </div>
        <div style='margin-left: 20px;'>
          <h6 class='my-0'><?php echo $_SESSION['username'] ?></h6>
          <a href='./logout.php'>Log uit</a>
        </div>
      </li>
    </ul>

    <?php

      }

    ?>

    <ul class="list-group mb-3" class="recent-orders" style="border-radius: 0px;">
      <li class="list-group-item d-flex justify-content-start lh-condensed">
      <div>
          <img src="https://crafatar.com/avatars/7ca041a4-d52b-4b87-bca1-c25d083f54b4?size=40&overlay">
      </div>
      <div style="margin-left: 20px;">
          <h6 class="my-0">BuddieDJ</h6>
          <small class="text-muted">Iron Rank</small>
      </div>
      </li>
      <li class="list-group-item d-flex justify-content-start lh-condensed">
      <div>
          <img src="https://crafatar.com/avatars/7ca041a4-d52b-4b87-bca1-c25d083f54b4?size=40&overlay">
      </div>
      <div style="margin-left: 20px;">
          <h6 class="my-0">BuddieDJ</h6>
          <small class="text-muted">Iron Rank</small>
      </div>
      </li>
      <li class="list-group-item d-flex justify-content-start lh-condensed">
      <div>
          <img src="https://crafatar.com/avatars/7ca041a4-d52b-4b87-bca1-c25d083f54b4?size=40&overlay">
      </div>
      <div style="margin-left: 20px;">
          <h6 class="my-0">BuddieDJ</h6>
          <small class="text-muted">Iron Rank</small>
      </div>
      </li>
      <li class="list-group-item d-flex justify-content-start lh-condensed">
      <div>
          <img src="https://crafatar.com/avatars/7ca041a4-d52b-4b87-bca1-c25d083f54b4?size=40&overlay">
      </div>
      <div style="margin-left: 20px;">
          <h6 class="my-0">BuddieDJ</h6>
          <small class="text-muted">Iron Rank</small>
      </div>
      </li>


    </ul>

  </div>

  </div>
  </div>

  <script>
paypal.Buttons({

      onInit: function(data, actions) {

          actions.disable();

          var firstname = document.getElementById('firstName')
          var lastname = document.getElementById('lastName')
          var email = document.getElementById('email')
          var address = document.getElementById('address')
          var address2 = document.getElementById('address2')
          var country = document.getElementById('country')
          var state = document.getElementById('state')
          var zip = document.getElementById('zip')

          document.querySelector('#check').addEventListener('change', function(event) {

          if (event.target.checked) {
              if(firstname.value.length == 0 || lastname.value.length == 0 || email.value.length == 0 || address.value.length == 0 || country.value.length == 0 || state.value.length == 0 || zip.value.length == 0) {
                  actions.disable();
                 document.querySelector('#error').style.display = 'block';
              } else {
                  actions.enable();
              }
          } else {
              actions.disable();
          }
          });
      },

      // onClick is called when the button is clicked
      onClick: function() {

        // Show a validation error if the checkbox is not checked
        if (!document.querySelector('#check').checked) {
            document.querySelector('#error').style.display = 'block';
        }
      },
      createOrder: function(data, actions) {
        // This function sets up the details of the transaction, including the amount and line item details.
        return actions.order.create({
          purchase_units: [{
            amount: {
              value: '{{ price }}'
            }
          }]
        });
      },
      onApprove: function(data, actions) {
        // This function captures the funds from the transaction.
        return actions.order.capture().then(function(details) {
          // This function shows a transaction success message to your buyer.
              {#window.location.href = '/complete'#}
              {#sessionStorage.setItem('firstname', firstname.value);#}
              {#sessionStorage.setItem('lastname', lastname.value);#}
              {#sessionStorage.setItem('email', email.value);#}
              {#sessionStorage.setItem('address', address.value);#}
              {#sessionStorage.setItem('address2', address2.value);#}
              {#sessionStorage.setItem('country', country.value);#}
              {#sessionStorage.setItem('state', state.value);#}
              {#sessionStorage.setItem('zip', zip.value);#}
              var firstname = document.getElementById('firstName').value()
              var lastname = document.getElementById('lastName').value()
              var email = document.getElementById('email').value()
              var address = document.getElementById('address').value()
              var address2 = document.getElementById('address2').value()
              var country = document.getElementById('country').value()
              var state = document.getElementById('state').value()
              var zip = document.getElementById('zip').value()

              window.location.href = '/mail?firstname=' + firstname; + '&lastname=' + lastname + '&email=' + email + 'address=' + address + '&country=' + country + '&state=' + state + '&zip=' + zip;

        });
      },
        onCancel: function (data) {
          window.location.href = '/cancelled'
        }

}).render('#paypal-button-container');

  </script>

<?php

  include('./includes/footer.php');

?>
