<?php


?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CraftingShop - Installer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <link rel="stylesheet" href="./assets/css/style.css">
    <style media="screen">
    </style>
  </head>
  <body>

    <div class="container">
      <h2 style="margin-bottom: 15px">Installatie gelukt!</h2>
      <?php

      ini_set('display_errors', 0);
      ini_set('display_errors', false);

      ?>
      <br>
      <br>
      <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
      </svg>
      <br><br>
      <div class="FormButtons">
        <form action="../actions/removeInstaller.php" method="post">
          <button type="submit" name="SUBMIT_INSTALL">Ga verder</button>
        </form>
      </div>
    </div>
  </body>
</html>
