<?php

if(isset($_POST['SUBMIT_INSTALL'])) {
  $php_version = phpversion();
  if($php_version<5) {
    $error = true;
    $php_error = "PHP version = $php_version - update PHP om verder te gaan..";
  }

  function getMySQLVersion() {
    $output = shell_exec('mysql -V');
    preg_match('@[0-9]+\.[0-9]+\.[0-9]+@', $output, $version);
    return $version;
  }

  $mysql_version = getMySQLVersion();
  if($mysql_version < 5) {
    if($mysql_version == -1) $mysql_error = "MySQL versie word gecheckt bij de volgende stap..";
    else $mysql_error = "MySQL versie is $mysql_version. Versie 5 of nieuwer is nodig..";
  }

  if(ini_get('safe_mode')) {
    $error = true;
    $safe_mode_error = "Zet PHP Safe Mode uit<br>";
  }

  $_SESSION['craftingshop_sessions_work'] = 1;
  if(empty($_SESSION['craftingshop_sessions_work'])) {
    $error = true;
    $session_error = "Sessies moeten aan staan..";
  }

  $db_error = false;
  if(!mysqli_connect($_POST['SQL_HOST'], $_POST['SQL_USERNAME'], $_POST['SQL_PASSWORD'])) {
    $db_error=true;
    $error_msg="Sorry, deze gegevens kloppen niet.";
  }

  $con = mysqli_connect($_POST['SQL_HOST'], $_POST['SQL_USERNAME'], $_POST['SQL_PASSWORD']);

  if(!$db_error and !mysqli_select_db($con, $_POST['SQL_DB'])) {
    $db_error=true;
    $error_msg="De host, gebruikersnaam en wachtwoord zijn correct.
    Maar er is iets mis met de gegeven database.
    Hier is de MySQL-fout: ". mysqli_error();
  } else {
    $query = file_get_contents('../../database/SQL/create_databases.sql');
    if ($con->multi_query($query)) {
        $con->close();
    } else {
        throw new Exception ($con->error);
    }

  }

  $connect_code="<?php
    define('APP_NAME','".$_POST['APP_NAME']."');

    define('SQL_HOST','".$_POST['SQL_HOST']."');
    define('SQL_USERNAME','".$_POST['SQL_USERNAME']."');
    define('SQL_PASSWORD','".$_POST['SQL_PASSWORD']."');
    define('SQL_DB','".$_POST['SQL_DB']."');

    define('LICENSE','".$_POST['LICENSE_CODE']."');

  ?>";

  if(!is_writable('../../config/config.include.php')) {
    $error_msg="<p>Sorry, I can't write to <b>inc/db_connect.php</b>.
    You will have to edit the file yourself. Here is what you need to insert in that file:<br /><br />
    <textarea rows='5' cols='50' onclick='this.select();'>$connect_code</textarea></p>";
  }
  else {
    $fp = fopen('../../config/config.include.php', 'w') or die("Unable to open file!");
    fwrite($fp, $connect_code) or die ('cannot write');
    fclose($fp);
  }

}

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CraftingShop - Installer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <link rel="stylesheet" href="./assets/css/style.css">
  </head>
  <body>

    <div class="container">
      <h2 style="margin-bottom: 15px">Er is een fout opgetreden..</h2>
      <?php

      ini_set('display_errors', 0);
      ini_set('display_errors', false);

      if(!empty($php_error)) echo "<span class='error'>Update PHP om verder te gaan<br></span>";
      if(!empty($mysql_error)) echo "<span class='error'>Update MySQL om verder te gaan<br></span>";
      if(!empty($safe_mode_error)) echo "<span class='error'>Zet PHP Safe Mode uit<br></span>";
      if(!empty($session_error)) echo "<span class='error'>PHP Sessies zijn uitgeschakeld<br></span>";
      if(!empty($error_msg)) echo "<span class='error'>" . $error_msg . "</span>";

      if(empty($php_error) && empty($mysql_error) && empty($safe_mode_error) && empty($session_error) && empty($error_msg)) {
        header('Location: ./finished.php');
      }

      ?>
      <br><br><br>
      <div class="FormButtons">
        <button>
          <a href="./index.php" style="color: #fff;">Verwijder de installer</a>
        </button>
      </div>
    </div>
  </body>
</html>
