
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CraftingShop - Installer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <link rel="stylesheet" href="./assets/css/style.css">
  </head>

  <body>
    <div class="container">
      <img src="assets/img/concept-logo.png" width="400">

      <div class="animated">
        <h2>Installeer CraftingShop</h2>

        <p>Stel uw forum in door hieronder uw gegevens in te vullen. Als u problemen ondervindt, stuur dan uw probleem naar <a href="mailto:info@craftingshop.nl">info@craftingshop.nl</a>.</p>

          <form method="post" action="./install.php">
            <div id="error" style="display:none"></div>

            <div class="FormGroup">
              <div class="FormField">
                <label>App naam</label>
                <input name="APP_NAME">
              </div>
              <div class="FormField">
                <label>Licentie</label>
                <input name="LICENSE_CODE">
              </div>
            </div>

            <div class="FormGroup">
              <div class="FormField">
                <label>MySQL Host</label>
                <input name="SQL_HOST" value="localhost">
              </div>

              <div class="FormField">
                <label>MySQL Database</label>
                <input name="SQL_DB">
              </div>

              <div class="FormField">
                <label>MySQL Username</label>
                <input name="SQL_USERNAME">
              </div>

              <div class="FormField">
                <label>MySQL Password</label>
                <input type="password" name="SQL_PASSWORD">
              </div>

            </div>

            <div class="FormGroup">
              <div class="FormField">
                <label>Admin Username</label>
                <input name="ADMIN_USERNAME">
              </div>

              <div class="FormField">
                <label>Admin Email</label>
                <input name="ADMIN_EMAIL">
              </div>

              <div class="FormField">
                <label>Admin Password</label>
                <input type="password" name="ADMIN_PASSWORD">
              </div>

              <div class="FormField">
                <label>Confirm Password</label>
                <input type="password" name="ADMIN_CONFIRM_PASSWORD">
              </div>
            </div>

            <div class="FormButtons">
              <button type="submit" name="SUBMIT_INSTALL">Installeer CraftingShop</button>
            </div>
          </form>

<!-- <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script>
$(function() {
  $('form :input:first').select();

  $('form').on('submit', function(e) {
    e.preventDefault();

    var $button = $(this).find('button')
      .text('Please Wait...')
      .prop('disabled', true);

    $.post('', $(this).serialize())
      .done(function() {
        window.location.reload();
      })
      .fail(function(data) {
        $('#error').show().text('Something went wrong:\n\n' + data.responseText);

        $button.prop('disabled', false).text('Install Flarum');
      });

    return false;
  });
});
</script> -->
      </div>
    </div>
  </body>
</html>
