<?php

$dir = '../installer/';

function deleteFolder($path) {
    if(!empty($path) && is_dir($path) ){
        $dir  = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS); //upper dirs are not included,otherwise DISASTER HAPPENS :)
        $files = new RecursiveIteratorIterator($dir, RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $f) {if (is_file($f)) {unlink($f);} else {$empty_dirs[] = $f;} } if (!empty($empty_dirs)) {foreach ($empty_dirs as $eachDir) {rmdir($eachDir);}} rmdir($path);
    }
}

// Untag dit bij release of beta testing
// deleteFolder($dir);

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CraftingShop - Installer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <link rel="stylesheet" href="../installer/assets/css/style.css">
  </head>
  <body>

    <div class="container">
      <h2 style="margin-bottom: 15px">Installatie 100% voltooid</h2>
      <?php

      ini_set('display_errors', 0);
      ini_set('display_errors', false);

      ?>
      <p>De installatie van CraftingShop is voltooid, u kunt nu verder met het instellen van pakketten en betaal methodes in het admin paneel</p>
      <br>
      <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
      </svg>
      <br><br>
      <div class="FormButtons">
        <form action="../functions/removeFolder.php" method="post">
          <button type="submit" name="SUBMIT_INSTALL">Ga verder</button>
        </form>
      </div>
    </div>
  </body>
</html>
