<?php

  include('./includes/header.php');

?>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body" style="text-align: center">
              <div class="container">
            <h5><b>Voer Minecraft naam in</b></h5>
            <br>
                  <form action="./handlers/loginHandler.php" method="post">
                      <input type="text" id="inputPassword5" class="form-control" aria-describedby="passwordHelpBlock" name="username" >
                      <br>
                      <input class="btn btn-primary" type="submit" value="Ga verder" name="loginBtn" style="width: 100%; background: #325BA6; border-color: #325BA6;">
                  </form>
                  <br>
                  <?php
                    if(!empty($_GET['msg'])) {
                      $msg = $_GET['msg'];

                      if($msg === 'invalid') {
                        echo "<div class='text-danger'>Gebruikersnaam is niet gevonden bij Minecraft..</div>";
                      } elseif($msg === 'emptyfields') {
                        echo "<div class='text-danger'>Vul een gebruikersnaam in..</div>";
                      }
                    }
                  ?>
              </div>
      </div>
    </div>
    <BR>
  </div>
  </div>

<?php

  include('./includes/footer.php');

?>
