<?php
  require '../includes/mojang-api.class.php';
  $username = $_POST['username'];

  if(isset($_POST['loginBtn'])) {
    if(empty($username)) {
        header("Location: ../login.php?msg=emptyfields");
        exit();
    } else {

      $uuid = MojangAPI::getUuid($username);

      $valid = MojangAPI::isValidUuid($uuid);
      if($valid === true) {
        session_start();
        $_SESSION['username'] = $username;
        $_SESSION['uuid'] = $uuid;
        $_SESSION['loggedIn'] = True;
        echo $_SESSION['username'] . "<br>";
        echo $_SESSION['uuid'] . "<br>";
        echo $_SESSION['loggedIn'];
        header("Location: ../index.php");
      } else {
        $_SESSION['loggedIn'] = false;
        header("Location: ../login.php?msg=invalid");
      }
    }
  }

?>
