<?php

  include('./includes/header.php');
  // if(!isset($_SESSION['username'])) {
  //     header("Location: ./login.php");
  // }

?>

  <div class="row">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <h5><b>Welkom op de DarkMC Shop</b></h5>
			    <p class="card-text">Welkom op de DarkMC Server shop. Hier zijn allerlei verschillende items te koop die te gebruiken zijn op de server. Alle opbrengsten zullen gebruikt worden voor het development en onderhoud van de Server. Let bij het invullen van je Minecraft naam op hoofdletters, het kan 5 tot 60 min duren voordat jij aankopen ontvangt op de server.</p>

			    <br>
			  	<h5><b>Algemene voorwaarde</b></h5>
			  	<p class="card-text">Onze algemene voorwaarden zijn, voordat u overgaat tot betaling, volledig in te zien. Aankopen in onze shop vallen onder digitale inhoud en kunnen niet herroepen worden als deze geleverd is. Het kopen van een artikel in onze shop maakt spelers niet onschendbaar voor server regels. Indien een speler de regels overtreedt, zal deze gelijk behandeld worden, ongeacht of dit een betalende speler betreft. DarkMC heeft in het kader van de nieuwe wet op de privacy een privacyverklaring opgesteld. Deze is te vinden op onze Website! Als je een aankoop doet via onze Webstore moet jij hier 18+ voor zijn of toestemming hebben van een ouder of verzorger.</p>

			  	<br>
			  	<h5><b>Disclaimer</b></h5>
			  	<p class="card-text">DarkMC en alle daarbij behorende activiteiten zijn niet verbonden met of gelinkt aan Mojang. Minecraft is © en een handelsmerk van Mojang AB. Voor alle vragen en problemen neem contact op met onze klantenservice!</p>

			  	<br>
			  	<h5><b>Klantenservice</b></h5>
			  	<p class="card-text">Bij vragen of opmerkingen met betrekking tot een aankoop, verwijzen wij door naar onze klantenservice. Wij beantwoorden Allene vragen over betalingen, alle andere e-mails worden niet beantwoord. Via het forum zijn de medewerkers van ons team bereikbaar en proberen wij zo snel mogelijk jouw vraag te beantwoorden. Klik op de knop hier beneden en maak een account aan op onze site om een ticket te openen. Via Discord is het ook mogelijk om in contact te komen. Join hiervoor onze server.</p>

          <a href="https://discord.gg/aTw2x89" class="btn btn-success" target="_blank"><i class="fab fa-discord"></i> Discord</a>
        </div>
      </div>
    </div>

    <div class="col-md-4 order-md-2 mb-4">

      <?php

        if(isset($_SESSION['loggedIn'])) {

      ?>

      <ul class='list-group mb-3' class='recent-orders' style='border-radius: 0px;'>
        <li class='list-group-item d-flex justify-content-start lh-condensed'>
          <div>
            <img src='https://crafatar.com/avatars/<?php echo $_SESSION['uuid']; ?>?size=40&overlay'>
          </div>
          <div style='margin-left: 20px;'>
            <h6 class='my-0'><?php echo $_SESSION['username'] ?></h6>
            <a href='./logout.php'>Log uit</a>
          </div>
        </li>
      </ul>

      <?php

        }

      ?>

		  <ul class="list-group mb-3" class="recent-orders" style="border-radius: 0px;">
        <li class="list-group-item d-flex justify-content-start lh-condensed">
        <div>
            <img src="https://crafatar.com/avatars/7ca041a4-d52b-4b87-bca1-c25d083f54b4?size=40&overlay">
        </div>
        <div style="margin-left: 20px;">
            <h6 class="my-0">BuddieDJ</h6>
            <small class="text-muted">Iron Rank</small>
        </div>
        </li>
        <li class="list-group-item d-flex justify-content-start lh-condensed">
        <div>
            <img src="https://crafatar.com/avatars/7ca041a4-d52b-4b87-bca1-c25d083f54b4?size=40&overlay">
        </div>
        <div style="margin-left: 20px;">
            <h6 class="my-0">BuddieDJ</h6>
            <small class="text-muted">Iron Rank</small>
        </div>
        </li>
        <li class="list-group-item d-flex justify-content-start lh-condensed">
        <div>
            <img src="https://crafatar.com/avatars/7ca041a4-d52b-4b87-bca1-c25d083f54b4?size=40&overlay">
        </div>
        <div style="margin-left: 20px;">
            <h6 class="my-0">BuddieDJ</h6>
            <small class="text-muted">Iron Rank</small>
        </div>
        </li>
        <li class="list-group-item d-flex justify-content-start lh-condensed">
        <div>
            <img src="https://crafatar.com/avatars/7ca041a4-d52b-4b87-bca1-c25d083f54b4?size=40&overlay">
        </div>
        <div style="margin-left: 20px;">
            <h6 class="my-0">BuddieDJ</h6>
            <small class="text-muted">Iron Rank</small>
        </div>
        </li>


		  </ul>

    </div>
  </div>

<?php

  include('./includes/footer.php');

?>
