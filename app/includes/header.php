<?php

session_start();

require('../config/config.include.php');
include('./actions/checkInstaller.php');


?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <base href="/craftingshop/">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./static/css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-Bfad6CLCknfcloXFOyFnlgtENryhrpZCe29RTifKEixXQZ38WheV+i/6YWSzkz3V" crossorigin="anonymous">


	<link rel="apple-touch-icon" sizes="57x57" href="./static/img/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="./static/img/favicon//apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="./static/img/favicon//apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="./static/img/favicon//apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="./static/img/favicon//apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="./static/img/favicon//apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="./static/img/favicon//apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="./static/img/favicon//apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="./static/img/favicon//apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="./static/img/favicon//android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="./static/img/favicon//favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="./static/img/favicon//favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="./static/img/favicon//favicon-16x16.png">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="./static/img/favicon//ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
    <title><?php echo APP_NAME; ?> - Shop</title>

  </head>
  <body>
  	<div class="header">
  		<div class="logo">
            <a href="/">
  			    <img src="./static/img/logo/logo.png" class="animation">
            </a>
  		</div>
        <nav class="navbar navbar-expand-md navbar-dark blue">
            <div class="container">
                <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="./">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="./products/ranks">Ranks</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="./products/rankup">Rankup</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="./products/crates">Crates</a>
                        </li>
        <!-- {#                {% if not loggedIn %}#}
        {#                <li class="nav-item">#}
        {#                    <a href="/login" class="nav-link">Login</a>#}
        {#                </li>#}
        {#                {% else %}#}
        {#                <li class="nav-item">#}
        {#                    <a href="/logout" class="nav-link">Loguit</a>#}
        {#                </li>#}
        {#                {% endif %}#} -->
                    </ul>
                </div>
                <div class="mx-auto order-0 ">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="./cart">
                                <i class="fa badge" style="font-size:24px" value="2">&#xf07a;</i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


      <!-- Discount bar -->

    	<div class="sale-bar">
  		    SALE! 25% OP ALLES
 		  </div>

  	</div>
  	<br>

    <div class="container">
