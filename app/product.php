<?php
  include('./includes/header.php');
  if(!isset($_SESSION['loggedIn'])) {
    header("Location: ../login.php");
  }
?>

<div class="row">
  <div class="col-md-8">
    <div class="card">
      <div class="card-body">
        <h5><b>{{ name }}</b></h5>
              <p>{{ description }}</p>
              <br>
              <h4 class="product-price">€ {{ price }},00</h4>
                <br>
                <br>
              <a href="/addtocart/?id={{ id }}" class="btn btn-info" style="float: right; border-radius: 0; background: #325BA6; border-color: #325BA6;">Voeg toe aan winkelwagen</a>
      </div>
    </div>
    <BR>
  </div>
  <div class="col-md-4 order-md-2 mb-4">
    <?php

      if(isset($_SESSION['loggedIn'])) {

    ?>

    <ul class='list-group mb-3' class='recent-orders' style='border-radius: 0px;'>
      <li class='list-group-item d-flex justify-content-start lh-condensed'>
        <div>
          <img src='https://crafatar.com/avatars/<?php echo $_SESSION['uuid']; ?>?size=40&overlay'>
        </div>
        <div style='margin-left: 20px;'>
          <h6 class='my-0'><?php echo $_SESSION['username'] ?></h6>
                  <a href='./logout.php'>Log uit</a>
        </div>
      </li>
    </ul>

    <?php

      }

    ?>
    <ul class="list-group mb-3" class="recent-orders" style="border-radius: 0px;">
      <li class="list-group-item d-flex justify-content-start lh-condensed">
      <div>
          <img src="https://crafatar.com/avatars/7ca041a4-d52b-4b87-bca1-c25d083f54b4?size=40&overlay">
      </div>
      <div style="margin-left: 20px;">
          <h6 class="my-0">BuddieDJ</h6>
          <small class="text-muted">Iron Rank</small>
      </div>
      </li>
      <li class="list-group-item d-flex justify-content-start lh-condensed">
      <div>
          <img src="https://crafatar.com/avatars/7ca041a4-d52b-4b87-bca1-c25d083f54b4?size=40&overlay">
      </div>
      <div style="margin-left: 20px;">
          <h6 class="my-0">BuddieDJ</h6>
          <small class="text-muted">Iron Rank</small>
      </div>
      </li>
      <li class="list-group-item d-flex justify-content-start lh-condensed">
      <div>
          <img src="https://crafatar.com/avatars/7ca041a4-d52b-4b87-bca1-c25d083f54b4?size=40&overlay">
      </div>
      <div style="margin-left: 20px;">
          <h6 class="my-0">BuddieDJ</h6>
          <small class="text-muted">Iron Rank</small>
      </div>
      </li>
      <li class="list-group-item d-flex justify-content-start lh-condensed">
      <div>
          <img src="https://crafatar.com/avatars/7ca041a4-d52b-4b87-bca1-c25d083f54b4?size=40&overlay">
      </div>
      <div style="margin-left: 20px;">
          <h6 class="my-0">BuddieDJ</h6>
          <small class="text-muted">Iron Rank</small>
      </div>
      </li>


    </ul>

  </div>
  </div>
  </div>

<?php

  include('./includes/footer.php');

?>
